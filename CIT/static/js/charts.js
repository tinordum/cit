var table = $("#tbody");
var myChart = echarts.init(document.getElementById('main'));
var chart_high = echarts.init(document.getElementById('chart_high'));
var pop = [];
var date_array = [];
var average_value = [];

table.find('tr').each(function(i, el) {
    var $tds = $(this).find('td');
    date_array.push($tds.eq(1).html())
    average_value.push(parseInt($tds.eq(4).text() - parseInt($tds.eq(5).text())))
    pop.push([parseInt($tds.eq(2).text()), parseInt($tds.eq(3).text()), parseInt($tds.eq(4).text()), parseInt($tds.eq(5).text())],)
});

option = {
    title: {
        text: 'The history of the quotes of the BTC on January 1, 2018',
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        }
    },
    xAxis: {
        data: date_array
    },
    yAxis: {
        scale: true
    },
    dataZoom: [
        {
            type: 'inside',
            start: 50,
            end: 100
        },
        {
            show: true,
            type: 'slider',
            y: '90%',
            start: 50,
            end: 100
        }
    ],
    series: [{
        name: 'data',
        type: 'k',
        data: pop,
            markPoint: {
                label: {
                    normal: {
                        formatter: function (param) {
                            return param != null ? Math.round(param.value) : '';
                        }
                    }
                },
                data: [
                    {
                        name: 'highest value',
                        type: 'max',
                        valueDim: 'highest'
                    },
                    {
                        name: 'lowest value',
                        type: 'min',
                        valueDim: 'lowest'
                    },
                    {
                        name: 'average value on close',
                        type: 'average',
                        valueDim: 'close'
                    }
                ],
                tooltip: {
                    formatter: function (param) {
                        return param.name + '<br>' + (param.data.coord || '');
                    }
                }
            },
            markLine: {
                data: [
                    [
                        {
                            name: 'from lowest to highest',
                            type: 'min',
                            valueDim: 'lowest',
                            symbol: 'circle',
                            symbolSize: 10,
                            label: {
                                normal: {show: false},
                                emphasis: {show: false}
                            }
                        },
                        {
                            type: 'max',
                            valueDim: 'highest',
                            symbol: 'circle',
                            symbolSize: 10,
                            label: {
                                normal: {show: false},
                                emphasis: {show: false}
                            }
                        }
                    ],
                    {
                        name: 'min line on close',
                        type: 'min',
                        valueDim: 'close'
                    },
                    {
                        name: 'max line on close',
                        type: 'max',
                        valueDim: 'close'
                    }
                ]
            }
    }]
};

high_option = {
    title: {
        text: 'Average value between max and low',
    },

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        }
    },
    xAxis: {
        data: date_array
    },
    yAxis: {
        scale: true
    },
    dataZoom: [
        {
            type: 'inside',
            start: 50,
            end: 100
        },
        {
            show: true,
            type: 'slider',
            y: '90%',
            start: 50,
            end: 100
        }
    ],
    series: [{
        name: 'Average value',
        type: 'bar',
        data: average_value,
        animationDelay: function (idx) {
            return idx * 10 + 100;
        }
    }]
};

myChart.setOption(option);
chart_high.setOption(high_option);