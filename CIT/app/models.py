from django.db import models

class Data(models.Model):
    id = models.AutoField(primary_key=True)
    time = models.IntegerField()
    open = models.IntegerField()
    high = models.IntegerField()
    low = models.IntegerField()
    close = models.IntegerField()

    class Meta:
        db_table = 'data'
        ordering = ['time']
