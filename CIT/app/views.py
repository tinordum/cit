from django.views.generic import ListView, UpdateView
from . import models



# Create your views here.

class DataView(ListView):
    template_name = 'data.html'
    model = models.Data
    context_object_name = 'values'

class DataEditView(UpdateView):
    model = models.Data
    fields = ['time', 'open', 'high', 'low', 'close']