import csv
import psycopg2

conn = psycopg2.connect("host=localhost dbname=cit user=cituser password=qwe123qwe")
cur = conn.cursor()
cur.execute("""
CREATE TABLE data(
    id serial PRIMARY KEY,
    time integer,
    open integer,
    high integer, 
    low integer,
    close integer
)
""")
with open('data.txt') as f:
    reader = csv.reader(f)
    next(reader)
    for i in f:
        i = i.strip().split(',')
        time = i[0]
        open = i[1]
        high = i[2]
        low = i[3]
        close = i[4]
        cur.execute(
            "INSERT INTO data(time, open, high, low, close) VALUES (%s, %s, %s, %s, %s)", (time, open, high, low, close)
        )
        conn.commit()
conn.close()